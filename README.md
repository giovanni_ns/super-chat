# Super-Chat

the Super-Chat project is a unified group web chat. It is a simple PHP project to learn how PHP works and start developing better applications.

This project was started when I had a need to talk to another person on the same network. Using PHP Server I was able to create a super simple version of Super-Chat. After some development, this is the latest version of it

## Dependences

- MariaDB
- PHP
- PHP Server

### After dependences

> You need execute the SQL file `/superchat/database/models/database.sql`
> If you want stay default configuration it's ready to use!

### Customize

> If you changed the values on que SQL schema file you will need edit the connection file, is located in `/superchat/database/conn.inc.php`
>
> To change the secret password to enter the Super-Chat you need just change it in `/login/verify.php` inside of the If statement
