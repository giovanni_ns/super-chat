<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <title>Super-Chat</title>
</head>

<body>
    <div>
        <form action="/login/verify.php" method="post">
            <div class="columns">
                <div class="column"></div>
                <div class="column">
                    <section class="section is-large">
                        <h1 class="title is-1">Super-Chat</h1>
                        <div class="field is-grouped">
                            <p class="control is-expanded">
                                <input class="input" type="password" name="pass" value="" required placeholder="Coloque a senha.">
                            </p>
                            <p class="control">
                                <a class="button is-warning">
                                    Entrar
                                </a>
                            </p>
                        </div>
                    </section>
                </div>
                <div class="column"></div>
            </div>
        </form>
    </div>

</body>

</html>