function loadXMLDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("chatbox").innerHTML =
        this.responseText;
    }
  };
  xhttp.open("GET", "./database/get.php", true);
  xhttp.send();
}

setInterval(function () {
  loadXMLDoc();
  $("textarea").focus();
}, 200)

window.onload = loadXMLDoc;

var form = $("form");
form.submit(function (e) {
  e.preventDefault();
  var msg = $("#fofo").serialize();
  if (msg != "") {
    $.post(
      "/superchat/database/send.php",
      msg,
    );
    $("#fofo").val("");
  } else {
    alert("O Textarea está vázio!");
    $("#fofo").focus();
  }
});
