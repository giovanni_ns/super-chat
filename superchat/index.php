<?php
require '../sessions/verify_session.inc.php';
require '../helper/helper.php';
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <title>Super-Chat</title>
</head>

<body>

    <section class="hero">
        <div class="hero-body">
            <p class="title">
                Bem-vindo!
            </p>
            <p class="subtitle">
                Para entrar no chat, por favor, informe o seu nome de usuário.
                <span class="help">Não recomendamos utilizar o próprio nome</span>
            </p>
        </div>
    </section>
    <div>
        <div class="container">
            <form action="/superchat/chat.php" method="get">
                <label class="label" for="name">Usuário</label>
                <div class="field is-grouped">
                    <p class="control is-expanded">
                        <input class="input" type="text" name="name" required autocomplete="off" placeholder="<?= plho_name() ?>">
                    </p>
                    <p class="control">
                        <input class="button is-success" type="submit" value="Entrar" />
                    </p>
                </div>
            </form>
        </div>

    </div>
</body>

</html>