<?php
require '../database/conn.inc.php';

$result = $conn->query("SELECT * FROM (SELECT * FROM chat ORDER BY id DESC LIMIT 8) sub ORDER BY id ASC;");
try {

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $name = $row['rem'];
            $msg = $row['msg'];
            $clock = $row['time']; ?>
            <div class="box">
                <strong><?= htmlentities($name) ?></strong>
                <hr>
                <p><?= htmlentities($msg) ?></p>
                <div class="has-text-right">
                    <span class="tag is-rounded">
                        <?= htmlentities($clock) ?>
                    </span>
                </div>
            </div>
    <?php }
    } else {
        echo "0 results";
    }
} catch (Exception $e) { ?>
    <p class="has-text-centered">
    <h1 class="is-size-1">Oops...</h1>
    <?= $e ?>
    </p>
<?php } ?>