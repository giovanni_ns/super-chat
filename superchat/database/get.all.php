<?php
require './database/conn.inc.php';

$result = $conn->query("SELECT * FROM chat;");

try {

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $id = $row['id'];
            $name = $row['rem'];
            $msg = $row['msg'];
            $clock = $row['time']; ?>
            <div class="box">
                <strong><?= htmlentities($name) ?> - <?= htmlentities($id) ?></strong>
                <hr>
                <p class=""><?= htmlentities($msg) ?></p>
                <div class="has-text-right">
                    <span class="tag is-rounded">
                        <?= htmlentities($clock) ?>
                    </span>
                </div>
            </div>
    <?php }
    } else {
        echo "0 results";
    }
} catch (Exception $e) { ?>
    <p class="has-text-centered">
    <h1 class="is-size-1">Oops...</h1>
    <?= $e ?>
    </p>
<?php } ?>