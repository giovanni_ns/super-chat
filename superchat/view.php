<?php
require '../sessions/verify_session.inc.php';
require './database/conn.inc.php';

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <title>Super-Chat</title>
</head>

<body>
<div class="my-2">
        <nav class="breadcrumb is-centered has-bullet-separator" aria-label="breadcrumbs">
            <ul>
                <li><a href="./view.php">Todas</a></li>
                <li><a href="./chat.php?name=<?= $_SESSION['name'] ?>">Super-Chat</a></li>
                <li><a href="./index.php">Alterar usuário</a></li>
                <li class="is-active"><a href="#"> <?= $_SESSION['name'] ?></a></li>
            </ul>
        </nav>
    </div>
    <div class="container" id="chatbox">
        <?php
        include './database/get.all.php';
        ?>
    </div>

</body>

</html>