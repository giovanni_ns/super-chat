<?php
require '../sessions/verify_session.inc.php';
$name = $_GET['name'];
$_SESSION['name'] = $name;
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <meta http-equiv="refresh" content="3610">
    <title>Super-Chat</title>
</head>

<body>
    <div class="my-2">
        <nav class="breadcrumb is-centered has-bullet-separator" aria-label="breadcrumbs">
            <ul>
                <li><a href="./view.php">Todas</a></li>
                <li><a href="./index.php">Alterar usuário</a></li>
                <li class="is-active"><a href="#"> <?= $_SESSION['name'] ?></a></li>
            </ul>
        </nav>
    </div>
    <div class="container" id="chatbox"></div>

    <div class="container mx-auto my-3">
        <form method="post">

            <div class="field">
                <div class="control">
                    <input id="fofo" class="input is-medium" type="text" name="msg" placeholder="Sua mensagem aqui!" autocomplete="off">
                </div>
            </div>

            <div class="field">
                <p class="control">
                    <button id="btnsend" class="button is-warning" type="submit">Enviar</button>
                </p>
            </div>

        </form>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js" charset="utf-8"></script>
    <script src="./javascript/script.js"></script>
</body>

</html>